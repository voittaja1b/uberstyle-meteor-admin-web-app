AdminCollectionsCount   = new Mongo.Collection('adminCollectionsCount');

client_profile          = new Mongo.Collection("client_profile");

handyman_profile        = new Mongo.Collection("handyman_profile");

handyman_availability   = new Mongo.Collection("handyman_availability");

services                = new Mongo.Collection("services");

service_request         = new Mongo.Collection("service_request");

invoice                 = new Mongo.Collection("invoice");

payments                = new Mongo.Collection("payments");

chat                    = new Mongo.Collection("chat");

chat_messages           = new Mongo.Collection("chat_messages");

feedbacks               = new Mongo.Collection("feedbacks");

notification_history    = new Mongo.Collection("notification_history");

nodes                   = new Mongo.Collection("nodes");
