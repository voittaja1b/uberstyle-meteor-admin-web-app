AdminTables = {};

adminTablesDom = '<"box"<"box-header"<"box-toolbar"<"pull-left"<lf>><"pull-right"p>>><"box-body"t>><r>';

adminEditButton = {
  data: '_id',
  title: 'Edit',
  createdCell: function(node, cellData, rowData) {
    return $(node).html(Blaze.toHTMLWithData(Template.adminEditBtn, {
      _id: cellData
    }));
  },
  width: '40px',
  orderable: false
};

adminDelButton = {
  data: '_id',
  title: 'Delete',
  createdCell: function(node, cellData, rowData) {
    return $(node).html(Blaze.toHTMLWithData(Template.adminDeleteBtn, {
      _id: cellData
    }));
  },
  width: '40px',
  orderable: false
};

adminEditDelButtons = [adminEditButton, adminDelButton];

defaultColumns = function() {
  return [
    {
      data: '_id',
      title: 'ID'
    }
  ];
};

adminTablePubName = function(collection) {
  return "admin_tabular_" + collection;
};

adminCreateTables = function(collections) {
  return _.each(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : false, function(collection, name) {
    var columns;
    _.defaults(collection, {
      showEditColumn: true,
      showDelColumn: true,
      showInSideBar: true
    });
    columns = _.map(collection.tableColumns, function(column) {
      var createdCell;
      if (column.template) {
        createdCell = function(node, cellData, rowData) {
          $(node).html('');
          return Blaze.renderWithData(Template[column.template], {
            value: cellData,
            doc: rowData
          }, node);
        };
      }
      return {
        data: column.name,
        title: column.label,
        createdCell: createdCell
      };
    });
    if (columns.length === 0) {
      columns = defaultColumns();
    }
    if (collection.showEditColumn) {
      columns.push(adminEditButton);
    }
    if (collection.showDelColumn) {
      columns.push(adminDelButton);
    }
    return AdminTables[name] = new Tabular.Table({
      name: name,
      collection: adminCollectionObject(name),
      pub: collection.children && adminTablePubName(name),
      sub: collection.sub,
      columns: columns,
      extraFields: collection.extraFields,
      dom: adminTablesDom,
      selector: collection.selector || function() {
        return {};
      }
    });
  });
};

adminCreateRoutes = function(collections) {
  _.each(collections, adminCreateRouteView);
  _.each(collections, adminCreateRouteNew);
  return _.each(collections, adminCreateRouteEdit);
};

adminCreateRouteView = function(collection, collectionName) {
  return Router.route("adminDashboard" + collectionName + "View", adminCreateRouteViewOptions(collection, collectionName));
};

adminCreateRouteViewOptions = function(collection, collectionName) {
  var options;
  options = {
    path: "/" + collectionName,
    template: "AdminDashboardViewWrapper",
    controller: "AdminController",
    data: function() {
      return {
        admin_table: AdminTables[collectionName]
      };
    },
    action: function() {
      return this.render();
    },
    onAfterAction: function() {
      Session.set('admin_title', collectionName);
      Session.set('admin_subtitle', 'View');
      Session.set('admin_collection_name', collectionName);
      return collection.routes != null ? (collection.routes.view != null ? collection.routes.view.onAfterAction : false) : false;
    }
  };
  return _.defaults(options, collection.routes != null ? collection.routes.view : false);
};

adminCreateRouteNew = function(collection, collectionName) {
  return Router.route("adminDashboard" + collectionName + "New", adminCreateRouteNewOptions(collection, collectionName));
};

adminCreateRouteNewOptions = function(collection, collectionName) {
  var options;
  options = {
    path: "/" + collectionName + "/new",
    template: "AdminDashboardNew",
    controller: "AdminController",
    action: function() {
      return this.render();
    },
    onAfterAction: function() {
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));
      Session.set('admin_subtitle', 'Create new');
      Session.set('admin_collection_page', 'new');
      Session.set('admin_collection_name', collectionName);
      return collection.routes != null ? (collection.routes.new != null ? collection.routes.new.onAfterAction : false) : false;
    },
    data: function() {
      return {
        admin_collection: adminCollectionObject(collectionName)
      };
    }
  };
  return _.defaults(options, collection.routes != null ? collection.routes.new : false);
};

adminCreateRouteEdit = function(collection, collectionName) {
  return Router.route("adminDashboard" + collectionName + "Edit", adminCreateRouteEditOptions(collection, collectionName));
};

adminCreateRouteEditOptions = function(collection, collectionName) {
  var options;
  options = {
    path: "/" + collectionName + "/:_id/edit",
    template: "AdminDashboardEdit",
    controller: "AdminController",
    waitOn: function() {
      Meteor.subscribe('adminCollectionDoc', collectionName, parseID(this.params._id));
      return collection.routes != null ? (collection.routes.edit != null ? collection.routes.edit.waitOn : false) : false;
    },
    action: function() {
      return this.render();
    },
    onAfterAction: function() {
      Session.set('admin_title', AdminDashboard.collectionLabel(collectionName));
      Session.set('admin_subtitle', 'Edit ' + this.params._id);
      Session.set('admin_collection_page', 'edit');
      Session.set('admin_collection_name', collectionName);
      Session.set('admin_id', parseID(this.params._id));
      Session.set('admin_doc', adminCollectionObject(collectionName).findOne({
        _id: parseID(this.params._id)
      }));
      return collection.routes != null ? (collection.routes.edit != null ? collection.routes.edit.onAfterAction : false) : false;
    },
    data: function() {
      return {
        admin_collection: adminCollectionObject(collectionName)
      };
    }
  };
  return _.defaults(options, collection.routes != null ? collection.routes.edit : false);
};

adminPublishTables = function(collections) {
  return _.each(collections, function(collection, name) {
    if (!collection.children) {
      return false;
    }
    return Meteor.publishComposite(adminTablePubName(name), function(tableName, ids, fields) {
      var extraFields;
      check(tableName, String);
      check(ids, Array);
      check(fields, Match.Optional(Object));
      extraFields = _.reduce(collection.extraFields, function(fields, name) {
        fields[name] = 1;
        return fields;
      }, {});
      _.extend(fields, extraFields);
      this.unblock();
      return {
        find: function() {
          this.unblock();
          return adminCollectionObject(name).find({
            _id: {
              $in: ids
            }
          }, {
            fields: fields
          });
        },
        children: collection.children
      };
    });
  });
};

// code to run on server at startup
Meteor.startup(function () {

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Iron.utils.debug = true;
    }

    if (Meteor.isClient) {
        if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
            AutoForm.debug();
        }
    }

    /**
     ** Settings for Logger
     */
    Log = new Logger();

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
      new LoggerConsole(Log, {}).enable({
        enable: true,
        filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
        client: true, /* This allows to call, but not execute on Client */
        server: true   /* Calls from client will be executed on Server */
      });
    }

    new LoggerMongo(Log, {}).enable({
      enable: true,
      filter: ['ERROR', 'FATAL', 'WARN'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
      client: false, /* This allows to call, but not execute on Client */
      server: true   /* Calls from client will be executed on Server */
    });

    //Section for running only on server
    if (Meteor.isServer) {
      Accounts.emailTemplates.siteName = Meteor.settings.public.sitename;
      Accounts.emailTemplates.from = Meteor.settings.private.email.from;

      Mailer.config({
          from: Meteor.settings.private.email.from,     // Default 'From:' address. Required.
          replyTo: Meteor.settings.private.email.replyTo,  // Defaults to `from`.
          routePrefix: 'emails',              // Route prefix.
          baseUrl: process.env.ROOT_URL,      // The base domain to build absolute link URLs from in the emails.
          testEmail: null,                    // Default address to send test emails to.
          logger: Meteor.log,                 // Injected logger (see further below)
          silent: false,                      // If set to `true`, any `Logger.info` calls won't be shown in the console to reduce clutter.
          addRoutes: Meteor.settings.public.debug, // Add routes for previewing and sending emails. Defaults to `true` in development.
          language: 'html',                    // The template language to use. Defaults to 'html', but can be anything Meteor SSR supports (like Jade, for instance).
          plainText: true,                     // Send plain text version of HTML email as well.
          plainTextOpts: {}                   // Options for `html-to-text` module. See all here: https://www.npmjs.com/package/html-to-text
      });

      /**
      * Debug of SimpleSchema
      */
      if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        SimpleSchema.debug = true
      }
    }

    adminCreateTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : false);
    adminCreateRoutes(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : false);
    if (Meteor.isServer) {
      adminPublishTables(typeof AdminConfig !== "undefined" && AdminConfig !== null ? AdminConfig.collections : false);
    }
    if (AdminTables.Users) {
      return false;
    }
    return AdminTables.Users = new Tabular.Table({
      changeSelector: function(selector, userId) {
        var $or;
        $or = selector['$or'];
        $or && (selector['$or'] = _.map($or, function(exp) {
          var ref;
          if (((ref = exp.emails) != null ? ref['$regex'] : false) != null) {
            return {
              emails: {
                $elemMatch: {
                  address: exp.emails
                }
              }
            };
          } else {
            return exp;
          }
        }));
        return selector;
      },
      name: 'Users',
      collection: Meteor.users,
      columns: _.union([
        {
          data: '_id',
          title: 'Admin',
          createdCell: function(node, cellData, rowData) {
            return $(node).html(Blaze.toHTMLWithData(Template.adminUsersIsAdmin, {
              _id: cellData
            }));
          },
          width: '40px'
        }, {
          data: 'emails',
          title: 'Email',
          render: function(value) {
            return value[0].address;
          },
          searchable: true
        }, {
          data: 'emails',
          title: 'Mail',
          createdCell: function(node, cellData, rowData) {
            return $(node).html(Blaze.toHTMLWithData(Template.adminUsersMailBtn, {
              emails: cellData
            }));
          },
          width: '40px'
        }, {
          data: 'createdAt',
          title: 'Joined'
        }
      ], adminEditDelButtons),
      dom: adminTablesDom
    });
});
