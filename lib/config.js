AdminConfig = {
  name: 'Admin panel for It24',
  adminEmails: ['e@logvik.com'],
  nonAdminRedirectRoute: 'login',
  logoutRedirect: 'login',
  skin: 'green-light',
  collections: {
    client_profile: {
      label: 'Client profile',
      icon: 'user',
      omitFields: ['id_Client', 'password', 'cardnumber', 'cvc', 'expired', 'currentPosition', 'stripeToken', 'stripeCustomer'],
      tableColumns: [
       { label: 'Full name', name: 'fullName' },
       { label: 'Email', name: 'email' },
       { label: 'Phone', name: 'phone' }
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'aqua',
    },
    handyman_profile: {
      label: 'Handyman profile',
      icon: 'male',
      omitFields: ['id_Handyman', 'password', 'distance'],
      tableColumns: [
       { label: 'Full name', name: 'fullName' },
       { label: 'Email', name: 'email' },
       { label: 'Phone', name: 'phone' }
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'red',
    },
    services: {
      label: 'Services',
      icon: 'server',
      tableColumns: [
       { label: 'Name', name: 'name' },
       { label: 'Transport cost', name: 'transportCost' },
       { label: 'Rate hour', name: 'rateHour' }
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: false,
    },
    service_request: {
      label: 'Service requests',
      icon: 'book',
      omitFields: ['destinationPoint'],
      tableColumns: [
       { label: 'Id service request', name: '_id' },
       { label: 'Service', name: 'serviceRequestName()' },
       { label: 'Client', name: 'clientName()' },
       { label: 'Handyman', name: 'handymanName()' }
      ],
      extraFields: ['id_Service', 'id_Client', 'id_Handyman'],
      children: [
        {find: function(doc) {
            return services.find(doc.id_Service, {
              limit: 1
            });
          }
        },
        {find: function(doc) {
            return client_profile.find({id_Client: doc.id_Client}, {
              limit: 1
            });
          }
        },
        {find: function(doc) {
            return handyman_profile.find({id_Handyman: doc.id_Handyman}, {
              limit: 1
            });
          }
        }
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'yellow',
    },
    invoice: {
      label: 'Invoices',
      icon: 'file-text',
      omitFields: ['destinationPoint'],
      tableColumns: [
       { label: 'Id service request', name: 'id_ServiceRequest' },
       { label: 'Service request', name: 'serviceRequestName()' },
       { label: 'Total', name: 'total' },
       { label: 'Status', name: 'status' }
      ],
      children: [
        {find: function(doc) {
            var id_Service = service_request.findOne({_id: doc.id_ServiceRequest}, {
              limit: 1
            }).id_Service;
            return services.find({_id: id_Service}, {
              limit: 1
            });
          }
        },
        {find: function(doc) {
            return service_request.find({_id: doc.id_ServiceRequest}, {
              limit: 1
            });
          }
        },
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'green',
    },
    payments: {
      label: 'Payments',
      icon: 'money',
      tableColumns: [
       { label: 'Id invoice', name: 'id_Invoice' },
       { label: 'Id transaction', name: 'transactionId' },
       { label: 'Sum', name: 'sum' },
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'blue',
    },
    //chat: {},
    //chat_messages: {},
    feedbacks: {
      label: 'Feedbacks',
      icon: 'comment',
      tableColumns: [
       { label: 'Id service request', name: 'id_ServiceRequest' },
       { label: 'Name of handyman', name: 'handymanName()' },
       { label: 'Rating', name: 'rating' }
      ],
      extraFields: ['id_ServiceRequest'],
      children: [
        {find: function(doc) {
            var id_Handyman = service_request.findOne({_id: doc.id_ServiceRequest}, {
              limit: 1
            }).id_Handyman;
            return handyman_profile.find({id_Handyman: id_Handyman}, {
              limit: 1
            });
          }
        },
        {find: function(doc) {
            return service_request.find({_id: doc.id_ServiceRequest}, {
              limit: 1
            });
          }
        },
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'red',
    },
    nodes: {
      label: 'Nodes',
      icon: 'file-text-o',
      tableColumns: [
       { label: 'Title', name: 'title' },
       { label: 'Type', name: 'type' },
      ],
      showEditColumn: true, // Set to false to hide the edit button. True by default.
      showDelColumn: false, // Set to false to hide the edit button. True by default.
      showWidget: true,
      color: 'red',
    }
  }
}
