AutoForm.customCache = {};

AutoForm.addHooks(['admin_insert', 'admin_update', 'adminNewUser', 'adminUpdateUser', 'adminSendResetPasswordEmail', 'adminChangePassword'], {
  beginSubmit: function() {
    return $('.btn-primary').addClass('disabled');
  },
  endSubmit: function() {
    return $('.btn-primary').removeClass('disabled');
  },
  onError: function(formType, error) {
    return AdminDashboard.alertFailure(error.message);
  },
  docToForm: function(doc) {
    doc._id_Client = doc.id_Client;
    doc._id_Handyman = doc.id_Handyman;
    doc._id_Service = doc.id_Service;
    doc.id_Client = !_.isUndefined(client_profile.findOne({id_Client: doc.id_Client})) ? client_profile.findOne({id_Client: doc.id_Client}).fullName : "Some Client";
    doc.id_Handyman = !_.isUndefined(handyman_profile.findOne({id_Handyman: doc.id_Handyman})) ? handyman_profile.findOne({id_Handyman: doc.id_Handyman}).fullName : "Some Handyman";
    doc.id_Service = !_.isUndefined(services.findOne({_id: doc.id_Service})) ? services.findOne({_id: doc.id_Service}).name : "Some Service";
    AutoForm.customCache = doc;
    return doc;
  },
  formToDoc: function(doc) {
    doc.id_Client = AutoForm.customCache._id_Client;
    doc.id_Handyman = AutoForm.customCache._id_Handyman;
    doc.id_Service = AutoForm.customCache._id_Service;
    AutoForm.customCache = {};
    return doc;
  }
});

AutoForm.hooks({
  admin_insert: {
    onSubmit: function(insertDoc, updateDoc, currentDoc) {
      var hook;
      hook = this;
      Meteor.call('adminInsertDoc', insertDoc, Session.get('admin_collection_name'), function(e, r) {
        if (e) {
          return hook.done(e);
        } else {
          return adminCallback('onInsert', [Session.get('admin_collection_name', insertDoc, updateDoc, currentDoc)], function(collection) {
            return hook.done(null, collection);
          });
        }
      });
      return false;
    },
    onSuccess: function(formType, collection) {
      AdminDashboard.alertSuccess('Successfully created');
      return Router.go("/" + collection);
    }
  },
  admin_update: {
    onSubmit: function(insertDoc, updateDoc, currentDoc) {
      var hook;
      hook = this;
      Meteor.call('adminUpdateDoc', updateDoc, Session.get('admin_collection_name'), Session.get('admin_id'), function(e, r) {
        if (e) {
          return hook.done(e);
        } else {
          return adminCallback('onUpdate', [Session.get('admin_collection_name', insertDoc, updateDoc, currentDoc)], function(collection) {
            return hook.done(null, collection);
          });
        }
      });
      return false;
    },
    onSuccess: function(formType, collection) {
      AdminDashboard.alertSuccess('Successfully updated');
      return Router.go("/" + collection);
    }
  },
  adminNewUser: {
    onSuccess: function(formType, result) {
      AdminDashboard.alertSuccess('Created user');
      return Router.go('/Users');
    }
  },
  adminUpdateUser: {
    onSubmit: function(insertDoc, updateDoc, currentDoc) {
      Meteor.call('adminUpdateUser', updateDoc, Session.get('admin_id'), this.done);
      return false;
    },
    onSuccess: function(formType, result) {
      AdminDashboard.alertSuccess('Updated user');
      return Router.go('/Users');
    }
  },
  adminSendResetPasswordEmail: {
    onSuccess: function(formType, result) {
      return AdminDashboard.alertSuccess('Email sent');
    }
  },
  adminChangePassword: {
    onSuccess: function(operation, result, template) {
      return AdminDashboard.alertSuccess('Password reset');
    }
  }
});
